import pytest
from rest_framework.test import APIClient


@pytest.fixture(autouse=True)
def enable_db_access_for_all_tests(db):
    """Allows access to database for all tests.
    https://pytest-django.readthedocs.io/en/latest/faq.html#how-can-i-give-database-access-to-all-my-tests-without-the-django-db-marker
    """


@pytest.fixture
def anonymous_client_fixture():
    return APIClient()
