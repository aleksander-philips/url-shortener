from django.urls import path

from . import views

urlpatterns = [path("short/", views.ShortenerView.as_view(), name="shortener")]
