from django.http import Http404
from django.shortcuts import redirect
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView
from shortener.serializers import (
    CreateShortenedURLRequest,
    CreateShortenedURLResponse,
    ShortenerSerializerRequest,
    ShortenerSerializerResponse,
)
from shortener.services import ShortenedURLService


class ShortenerView(APIView):
    @swagger_auto_schema(
        query_serializer=ShortenerSerializerRequest,
        responses={
            status.HTTP_200_OK: ShortenerSerializerResponse,
            status.HTTP_404_NOT_FOUND: "The provided URL doesn't exist or is expired.",
        },
    )
    def get(self, request: Request):
        request_serializer = ShortenerSerializerRequest(
            data=request.query_params.dict()
        )
        request_serializer.is_valid(raise_exception=True)
        shortened_url = ShortenedURLService().get_active(**request_serializer.data)
        if shortened_url:
            response_data = ShortenerSerializerResponse(instance=shortened_url).data
            status_code = status.HTTP_200_OK
        else:
            response_data = None
            status_code = status.HTTP_404_NOT_FOUND

        return Response(response_data, status=status_code)

    @swagger_auto_schema(
        request_body=CreateShortenedURLRequest,
        responses={
            status.HTTP_201_CREATED: CreateShortenedURLResponse,
            status.HTTP_400_BAD_REQUEST: "Invalid data has been provided.",
        },
    )
    def post(self, request: Request):
        request_serializer = CreateShortenedURLRequest(data=request.data)
        request_serializer.is_valid(raise_exception=True)
        shortened_url = ShortenedURLService().create(**request_serializer.data)

        response_serializer = CreateShortenedURLResponse(instance=shortened_url)
        return Response(response_serializer.data, status=status.HTTP_201_CREATED)


def redirect_to_url(request, slug: str):
    """
    Get slug from the URL, increase clicks number and redirect an user.
    If the shortened URL doesn't exist or is expired then return HTTP 404.
    """
    service = ShortenedURLService()

    shortened_url = service.get_active(slug)
    if shortened_url:
        service.increment_clicks(shortened_url)
        return redirect(shortened_url.full_url)
    raise Http404()
