from django.db import models
from django.utils import timezone


class ShortenedURL(models.Model):
    full_url = models.URLField(max_length=500)
    shortened_slug = models.CharField(max_length=100)
    created_at = models.DateTimeField(default=timezone.now)
    clicks = models.PositiveIntegerField(default=0)
    expiration_date = models.DateTimeField()

    class Meta:
        indexes = [
            models.Index(fields=("created_at", "expiration_date")),
        ]
