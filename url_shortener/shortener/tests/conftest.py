import datetime

import pytest
import pytz
from django.conf import settings
from shortener.models import ShortenedURL

FULL_URL = "https://www.google.com/"
SHORTENED_SLUG = "mocked_slug"
CREATED_AT = datetime.datetime(2010, 5, 22, tzinfo=pytz.UTC)
CLICKS = 123


@pytest.fixture
def make_shortened_url():
    def fixture(
        *,
        full_url=None,
        shortened_slug=None,
        created_at=None,
        clicks=None,
        expiration_date=None
    ) -> ShortenedURL:
        data = {
            "full_url": full_url or FULL_URL,
            "shortened_slug": shortened_slug or SHORTENED_SLUG,
            "created_at": created_at or CREATED_AT,
            "expiration_date": expiration_date
            or CREATED_AT + datetime.timedelta(hours=settings.HOURS_TO_EXPIRATION),
            "clicks": clicks or CLICKS,
        }

        obj = ShortenedURL(**data)
        obj.save()
        obj.created_at = data["created_at"]
        return obj

    return fixture
