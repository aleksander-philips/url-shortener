import datetime

import pytest
import pytz
from conftest import CLICKS, CREATED_AT, SHORTENED_SLUG
from django.conf import settings
from django.forms import ValidationError
from shortener.exceptions import UniqueSlugNotFound
from shortener.services import ShortenedURLService


class TestShortenedURLService:
    def setup_class(self):
        self.service = ShortenedURLService()

    def test_create(self, freezer):
        url = "https://www.youtube.com/"
        mocked_date = datetime.datetime(2010, 2, 12, tzinfo=pytz.UTC)
        freezer.move_to(mocked_date)
        shortened_url = self.service.create(full_url=url)

        shortened_url.full_clean()

        assert shortened_url.full_url == url
        assert shortened_url.created_at == mocked_date
        assert shortened_url.expiration_date == mocked_date + datetime.timedelta(
            hours=settings.HOURS_TO_EXPIRATION
        )
        assert shortened_url.clicks is 0

    @pytest.mark.parametrize(
        "url", ["htt://www.youtube.com", "youtube.com", "http://www.youtube..com"]
    )
    def test_create_with_invalid_full_url(self, url):
        shortened_url = self.service.create(full_url=url)
        with pytest.raises(ValidationError):
            shortened_url.full_clean()

    def test_create_unique_slug_not_found(self, monkeypatch):
        monkeypatch.setattr("shortener.services.random_string", lambda _: "not_unique")
        self.service.create(full_url="https://www.youtube.com/")
        with pytest.raises(UniqueSlugNotFound):
            self.service.create(full_url="https://www.youtube.com/")

    def test_get_active(self, freezer, make_shortened_url):
        fixture = make_shortened_url()
        freezer.move_to(fixture.created_at + datetime.timedelta(minutes=10))

        # there's no slug with "none" value
        shortened_url = self.service.get_active("none")
        assert not shortened_url

        # find a correct slug which is not expired
        shortened_url = self.service.get_active(SHORTENED_SLUG)
        assert shortened_url.shortened_slug == SHORTENED_SLUG
        assert shortened_url.created_at == CREATED_AT
        assert shortened_url.expiration_date == CREATED_AT + datetime.timedelta(
            hours=settings.HOURS_TO_EXPIRATION
        )
        assert shortened_url.clicks == CLICKS

        # the URL is expired
        freezer.move_to(fixture.expiration_date + datetime.timedelta(minutes=10))
        shortened_url = self.service.get_active(SHORTENED_SLUG)
        assert not shortened_url

    def test_increment_clicks(self, make_shortened_url):
        shortened_url = make_shortened_url()
        old_value = shortened_url.clicks

        self.service.increment_clicks(shortened_url)

        assert shortened_url.clicks == old_value + 1
