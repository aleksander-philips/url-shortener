import datetime

import pytz
from conftest import FULL_URL
from django.conf import settings
from django.urls import reverse
from rest_framework import status
from shortener.models import ShortenedURL


def test_create_shortened_url(freezer, anonymous_client_fixture):
    created_at = datetime.datetime(2020, 10, 23, tzinfo=pytz.UTC)
    full_url = "http://youtube.com"
    freezer.move_to(created_at)
    url = reverse("shortener")

    data = {"full_url": full_url}
    response = anonymous_client_fixture.post(url, data=data)
    assert response.status_code == status.HTTP_201_CREATED

    data = response.json()

    shortened_url = ShortenedURL.objects.first()
    expiration_date = (
        created_at + datetime.timedelta(hours=settings.HOURS_TO_EXPIRATION)
    ).isoformat()
    expiration_date = expiration_date.split("+")[0] + "Z"

    assert data["expiration_date"] == expiration_date
    assert data["shortened_slug"] == shortened_url.shortened_slug


def test_create_invalid_full_url(anonymous_client_fixture):
    full_url = "htt://youtube.com"
    url = reverse("shortener")

    data = {"full_url": full_url}
    response = anonymous_client_fixture.post(url, data=data)
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert ["full_url"] == list(response.data)


def test_get_shortened_url(freezer, anonymous_client_fixture, make_shortened_url):
    shortened_url = make_shortened_url()
    freezer.move_to(shortened_url.created_at)
    url = reverse("shortener") + f"?shortened_slug={shortened_url.shortened_slug}"

    response = anonymous_client_fixture.get(url)
    assert response.status_code == status.HTTP_200_OK

    data = response.json()

    expiration_date = shortened_url.expiration_date.isoformat()
    expiration_date = expiration_date.split("+")[0] + "Z"

    assert data["full_url"] == shortened_url.full_url
    assert data["clicks"] == shortened_url.clicks
    assert data["expiration_date"] == expiration_date


def test_get_expired_shortened_url(
    freezer, anonymous_client_fixture, make_shortened_url
):
    shortened_url = make_shortened_url()
    freezer.move_to(shortened_url.expiration_date + datetime.timedelta(minutes=10))
    url = reverse("shortener") + f"?shortened_slug={shortened_url.shortened_slug}"

    response = anonymous_client_fixture.get(url)
    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_get_invalid_shortened_url(anonymous_client_fixture):
    url = reverse("shortener") + f"?shortened_slug=mock"
    response = anonymous_client_fixture.get(url)
    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_redirection(freezer, anonymous_client_fixture, make_shortened_url):
    shortened_url = make_shortened_url()
    freezer.move_to(shortened_url.created_at)
    url = reverse("shortened-redirect", args=(shortened_url.shortened_slug,))

    response = anonymous_client_fixture.get(url)
    assert response.status_code == status.HTTP_302_FOUND
    assert response.url == FULL_URL
