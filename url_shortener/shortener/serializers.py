from rest_framework import serializers
from shortener.models import ShortenedURL


class ShortenerSerializerRequest(serializers.ModelSerializer):
    class Meta:
        model = ShortenedURL
        fields = ("shortened_slug",)


class ShortenerSerializerResponse(serializers.ModelSerializer):
    class Meta:
        model = ShortenedURL
        fields = ("full_url", "expiration_date", "clicks")


class CreateShortenedURLRequest(serializers.ModelSerializer):
    class Meta:
        model = ShortenedURL
        fields = ("full_url",)


class CreateShortenedURLResponse(serializers.ModelSerializer):
    class Meta:
        model = ShortenedURL
        fields = ("shortened_slug", "expiration_date")
