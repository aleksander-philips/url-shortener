import random
import string


def random_string(length: int = 10) -> str:
    chars = random.sample(string.ascii_letters, length)
    return "".join(chars)
