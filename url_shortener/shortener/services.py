import datetime

from django.conf import settings
from django.utils import timezone
from shortener.exceptions import UniqueSlugNotFound
from shortener.models import ShortenedURL
from shortener.utils import random_string


class ShortenedURLService:
    def create(self, *, full_url: str) -> ShortenedURL:
        shortened_url = ShortenedURL(full_url=full_url)
        now = timezone.now()

        active_urls = ShortenedURL.objects.filter(
            created_at__lte=now, expiration_date__gte=now
        ).values_list("shortened_slug", flat=True)
        for _ in range(settings.UNIQUE_SLUG_TRIES):
            slug = random_string(settings.SLUG_LENGTH)
            if slug in active_urls:
                continue
            shortened_url.shortened_slug = slug
            shortened_url.expiration_date = now + datetime.timedelta(
                hours=settings.HOURS_TO_EXPIRATION
            )
            break
        else:
            raise UniqueSlugNotFound

        shortened_url.save()
        return shortened_url

    def get_active(self, shortened_slug: str) -> ShortenedURL | None:
        now = timezone.now()
        return ShortenedURL.objects.filter(
            shortened_slug=shortened_slug, created_at__lte=now, expiration_date__gte=now
        ).first()

    def increment_clicks(self, shortened_url: ShortenedURL) -> None:
        shortened_url.clicks += 1
        shortened_url.save()
