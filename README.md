# url shortener
The project is a simple proof of concept of the URL shortener mechanism.

The user can change a full URL to the shortened version handled by the app.
Shortened URL is active for limited time which can be set up by .env.


## How to run
1. setup .env file. The best way for start is to copy existing .env.example:
```bash
cd url_shortener
cp .env.example .env
```
2. Run docker compose services

```bash
docker compose up
```

3. Migrate models
```bash
docker compose run api python manage.py migrate
```

4. the app runs on http://localhost:8000


## API documentation
visit http://localhost:8000/swagger/