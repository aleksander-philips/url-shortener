args = "$(filter-out $@,$(MAKECMDGOALS))"
dc = docker compose -f docker-compose.yml

build-dev: requirements-dev _build

_build:
	docker compose build

makemigrations:
	docker compose run api python manage.py makemigrations

migrate:
	docker compose run api python manage.py migrate

up:
	$(dc) up -d db
	$(dc) up api --remove-orphans

down:
	$(dc) down

black:
	poetry run black url_shortener

isort:
	poetry run isort url_shortener --profile black

autoflake:
	poetry run autoflake --in-place --remove-all-unused-imports --remove-unused-variables --recursive url_shortener --exclude=__init__.py

format: autoflake isort black

requirements-dev:
	poetry export -f requirements.txt --output url_shortener/requirements/dev.txt --with dev

